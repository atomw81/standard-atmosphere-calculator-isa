# Standard Atmosphere Calculator-ISA

Standard Atmosphere calculator (ISA based): A java applet that takes geopotential altitude as input and gives the properties (Pressure-P, temperature-T and density-rho) of air at that altitude. Download the .jar file. (NOTE: you must have java installed on your device)

Linux: java -jar "Standard_Atmosphere_Calculator".jar

Windows: Right-click on the .jar file and open/run.